using System;
using System.Linq;
using System.Net;
using dns;
using Xunit;

namespace Tests
{
    public class ResponseMessageRead
    {
        [Fact]
        public void DecodesARecord()
        {
            const string hex
                = "aaaa81800001000100000000076578616d706c6503636f6d0000010001c00c00010001000020fb00045db8d822";
            var raw = HexToBytes(hex);
            var reader = new DatagramReader(raw);
            var response = ResponseMessage.CreateFromReader(reader);

            Assert.Equal(0xaaaa, response.Id);
            Assert.Equal(1, response.Records.Count);
            Assert.Equal("example.com", response.Records[0].DomainName);
            Assert.Equal(8443u, response.Records[0].InitialTimeToLive);
            Assert.Equal(IPAddress.Parse("93.184.216.34"), response.Records[0].RData.GetFirst().Address);
        }

        [Fact]
        public void DecodesAAAARecord()
        {
            const string hex =
                "444a8180000100010000000106676f6f676c6503636f6d00001c0001c00c001c00010000012b00102607f8b040060802000000000000200e0000291000000000000000";
            
            var reader = new DatagramReader(HexToBytes(hex));
            var response = ResponseMessage.CreateFromReader(reader);
            
            Assert.Equal(0x444a, response.Id);
            Assert.Equal(1, response.Records.Count);
            Assert.Equal("google.com", response.Records[0].DomainName);
            Assert.Equal(299u, response.Records[0].InitialTimeToLive);
            Assert.Equal(IPAddress.Parse("2607:f8b0:4006:802::200e"), response.Records[0].RData.GetSecond().Address);
        }

        [Fact]
        public void DecodesCNameRecord()
        {
            const string hex =
                "becb818000010001000100010377777703726974036564750000050001c00c0005000100000072000d0a77656230317777773031c010c0100006000100000e0f0022046e733161c01004726f6f74c010032c817300000e100000025800093a8000000e100000291000000000000000";
            
            var reader = new DatagramReader(HexToBytes(hex));
            var response = ResponseMessage.CreateFromReader(reader);
            
            Assert.Equal(0xbecb, response.Id);
            Assert.Equal(1, response.Records.Count);
            Assert.Equal("www.rit.edu", response.Records[0].DomainName);
            Assert.Equal(114u, response.Records[0].InitialTimeToLive);
            Assert.Equal("web01www01.rit.edu", response.Records[0].RData.GetThird().CanonicalName);
        }

        private static byte[] HexToBytes(string InputText)
        {
            return Enumerable.Range(0, InputText.Length)
                .Where(x => x % 2 == 0)
                .Select(x => Convert.ToByte(InputText.Substring(x, 2), 16))
                .ToArray();
        }
    }
}