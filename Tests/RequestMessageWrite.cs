using dns;
using Xunit;

namespace Tests
{
    public class RequestMessageWrite
    {
        [Fact]
        public void Size()
        {
            var m = new RequestMessage(10231, "snapchat.com", RecordType.A);
            var encoded = m.Encode();
            Assert.Equal(2 * 8 + 13 + 1, encoded.Length);
        }
    }
}