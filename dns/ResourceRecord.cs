using System;

namespace dns
{
    public class ResourceRecord
    {
        public string DomainName { get; }
        public uint InitialTimeToLive { get; }
        public Union3<ARecord, AaaaRecord, CnameRecord> RData { get; }

        private ResourceRecord(string domainName, uint initialTimeToLive, Union3<ARecord, AaaaRecord, CnameRecord> rData)
        {
            DomainName = domainName;
            InitialTimeToLive = initialTimeToLive;
            RData = rData;
        }

        public static ResourceRecord CreateFromReader(DatagramReader reader)
        {
            var name = reader.ReadDnsName();
            var type = reader.ReadType();
            reader.Skip(2);
            var ttl = reader.ReadU32();
            var length = reader.ReadU16();

            var RData = ParseRecord(reader, type);
            
            return new ResourceRecord(name, ttl, RData);
        }

        private static Union3<ARecord, AaaaRecord, CnameRecord> ParseRecord(DatagramReader reader, RecordType type)
        {
            switch (type)
            {
                case RecordType.A:
                    return new Union3<ARecord, AaaaRecord, CnameRecord>.Case1(new ARecord(reader.ReadIpV4Addr()));
                
                case RecordType.AAAA:
                    return new Union3<ARecord, AaaaRecord, CnameRecord>.Case2(new AaaaRecord(reader.ReadIpV6Addr()));
                
                case RecordType.CNAME:
                    return new Union3<ARecord, AaaaRecord, CnameRecord>.Case3(new CnameRecord(reader.ReadDnsName()));
                
                default:
                    throw new Exception("Invalid record type");
            }
        }
    }
}