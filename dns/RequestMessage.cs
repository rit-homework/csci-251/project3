using System;

namespace dns
{
    public class RequestMessage
    {
        public ushort Id { get; }
        public string QueryName { get; }
        public RecordType RecordType { get; }

        public RequestMessage(ushort id, string queryName, RecordType recordType)
        {
            Id = id;
            QueryName = queryName;
            RecordType = recordType;
        }

        public byte[] Encode()
        {
            var writer = new DatagramWriter();
            writer.WriteU16s(new ushort[] { Id, 0b00000001_00000000, 1, 0, 0, 0 });

            writer.WriteHostName(QueryName);
            writer.WriteU16((ushort)RecordType);
            writer.WriteU16(1);

            return writer.GetData();
        }
    }
}