using System;

namespace dns
{
    public abstract class Union3<TA, TB, TC>
    {
        public abstract T Match<T>(Func<TA, T> f, Func<TB, T> g, Func<TC, T> h);
        
        private Union3() { } 

        public sealed class Case1 : Union3<TA, TB, TC>
        {
            private readonly TA _item;
            public Case1(TA item) : base() { this._item = item; }
            public override T Match<T>(Func<TA, T> f, Func<TB, T> g, Func<TC, T> h)
            {
                return f(_item);
            }
        }

        public sealed class Case2 : Union3<TA, TB, TC>
        {
            private readonly TB _item;
            public Case2(TB item) { this._item = item; }
            public override T Match<T>(Func<TA, T> f, Func<TB, T> g, Func<TC, T> h)
            {
                return g(_item);
            }
        }

        public sealed class Case3 : Union3<TA, TB, TC>
        {
            private readonly TC _item;
            public Case3(TC item) { this._item = item; }
            public override T Match<T>(Func<TA, T> f, Func<TB, T> g, Func<TC, T> h)
            {
                return h(_item);
            }
        }

        public TA GetFirst() =>
            Match(
                t => t,
                _ => throw new Exception(),
                _ => throw new Exception()
            );
        
        public TB GetSecond() =>
            Match(
                _ => throw new Exception(),
                t => t,
                _ => throw new Exception()
            );
        
        public TC GetThird() =>
            Match(
                _ => throw new Exception(),
                _ => throw new Exception(),
                t => t
            );

        public void Match(Action<TA> f, Action<TB> g, Action<TC> h)
        {
            Match<object>(
                a => { f(a); return null; },
                b => { g(b); return null; },
                c => { h(c); return null; }
            );
        }
    }
}