using System.Net;

namespace dns
{
    public class AaaaRecord
    {
        public IPAddress Address { get; }

        public AaaaRecord(IPAddress address)
        {
            Address = address;
        }
    }
}