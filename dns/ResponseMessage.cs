using System.Collections.Generic;

namespace dns
{
    public class ResponseMessage
    {
        public ushort Id { get; }
        public IList<ResourceRecord> Records { get; }
        public IList<Question> Questions { get; }

        private ResponseMessage(ushort id, IList<ResourceRecord> records, IList<Question> questions)
        {
            Id = id;
            Records = records;
            Questions = questions;
        }

        public static ResponseMessage CreateFromReader(DatagramReader reader)
        {
            var id = reader.ReadU16();
            reader.Skip(2);
            var questionCount = reader.ReadU16();
            var answerCount = reader.ReadU16();
            var nsCount = reader.ReadU16();
            var arCount = reader.ReadU16();
            
            IList<Question> questions = new List<Question>();
            for (var questionIdx = 0; questionIdx < questionCount; questionIdx++)
            {
                questions.Add(Question.CreateFromReader(reader));
            }

            IList<ResourceRecord> records = new List<ResourceRecord>(answerCount);
            for (var answerIdx = 0; answerIdx < answerCount; answerIdx++)
            {
                records.Add(ResourceRecord.CreateFromReader(reader));
            }
            
            return new ResponseMessage(id, records, questions);
        }
    }
}