using System;
using System.Buffers.Binary;
using System.Linq;
using System.Text;

namespace dns
{
    public class DatagramWriter
    {
        private readonly byte[] _buffer = new byte[512];
        private int _index = 0;
        
        public ArraySegment<byte> Data => new ArraySegment<byte>(_buffer, 0, _index);

        private Span<byte> Span => _buffer;

        private Span<byte> TailSpan => Span.Slice(_index);

        private void WriteByte(byte b) => _buffer[_index++] = b;

        private void WriteBytes(Span<byte> bytes)
        {
            bytes.CopyTo(TailSpan);
            _index += bytes.Length;
        }

        public void WriteU16(ushort s)
        {
            BinaryPrimitives.WriteUInt16BigEndian(Span.Slice(_index), s);
            _index += 2;
        }

        public void WriteU16s(ushort[] ushorts)
        {
            foreach (var s in ushorts)
            {
                WriteU16(s);
            }
        }

        public void WriteHostName(string hostname)
        {
            var parts = hostname.Split(".").Select(part => Encoding.UTF8.GetBytes(part));
            foreach (var part in parts)
            {
                var len = (byte) part.Length;
                WriteByte(len);
                WriteBytes(part);
            }
            
            WriteByte(0);
        }

        public byte[] GetData()
        {
            return Data.ToArray();
        }
    }
}