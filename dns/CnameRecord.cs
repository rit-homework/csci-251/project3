namespace dns
{
    public class CnameRecord
    {
        public string CanonicalName { get; }

        public CnameRecord(string canonicalName)
        {
            CanonicalName = canonicalName;
        }
    }
}