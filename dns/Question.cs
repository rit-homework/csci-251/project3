namespace dns
{
    public class Question
    {
        public string QueryName { get; }
        public RecordType RecordType { get; }

        private Question(string queryName, RecordType recordType)
        {
            RecordType = recordType;
            QueryName = queryName;
        }

        public static Question CreateFromReader(DatagramReader reader)
        {
                var name = reader.ReadDnsName();
                var type = reader.ReadType();
                var klass = reader.ReadU16();
            
            return new Question(name, type);
        }
    }
}