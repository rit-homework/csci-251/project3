﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace dns
{
    class Program
    {
        private static readonly Random Random = new Random();

        static void Main(string[] args)
        {
            var parsedArgs = Arguments.Parse(args);
            if (parsedArgs == null)
            {
                return;
            }
            
            Request(parsedArgs.Type, parsedArgs.ToLookup, parsedArgs.DnsServer).Wait();
        }

        private static void PrintQuestionSection(ResponseMessage response, RecordType method, string toLookup)
        {
            Console.WriteLine(";; QUESTION SECTION:");
            if (response.Questions.Count == 0)
            {
                Console.WriteLine($";{toLookup}. IN {method}");
            }
            
            foreach (var question in response.Questions)
            {
                Console.WriteLine($";{question.QueryName}. IN {question.RecordType}");
            }
            Console.WriteLine();
        }

        private static void PrintAnswerSection(ResponseMessage response)
        {
            Console.WriteLine(";; ANSWER SECTION");
            if (response.Records.Count == 0)
            {
                Console.WriteLine("NO ANSWERS RECEIVED");
            }
            
            foreach (var record in response.Records)
            {
                var rdataPart = record.RData.Match(
                    aRecord => $"A {aRecord.Address}",
                    aaaaRecord => $"AAAA {aaaaRecord.Address}",
                    cnameRecord => $"CNAME {cnameRecord.CanonicalName}."
                );
                
                Console.WriteLine($"{record.DomainName}. {record.InitialTimeToLive} IN {rdataPart}");
            }
            Console.WriteLine();
        }

        private static async Task Request(RecordType method, string toLookup, IPEndPoint endPoint)
        {
            var id = (ushort) Random.Next(0xffff);

            var request = new RequestMessage(id, toLookup, method);
            var (response, length) = await MakeRequest(request, endPoint);
            
            Console.WriteLine($"; <<>> Not Dig <<>> {toLookup}");
            PrintQuestionSection(response, method, toLookup);
            PrintAnswerSection(response);

            Console.WriteLine($";; SERVER: {endPoint.Address}#{endPoint.Port}({endPoint.Address})");
            Console.WriteLine($";; WHEN: {DateTime.Now}");
            Console.WriteLine($";; MSG SIZE  rcvd: {length}");
        }
        
        private static async Task<(ResponseMessage, int)> MakeRequest(RequestMessage request, IPEndPoint endpoint)
        {
            var encoded = request.Encode();

            var client = new UdpClient();
            client.Connect(endpoint);
            await client.SendAsync(encoded, encoded.Length);

            (ResponseMessage, int) parsed;
            while (true)
            {
                var response = await client.ReceiveAsync();
                var reader = new DatagramReader(response.Buffer);
                var localParsed = ResponseMessage.CreateFromReader(reader);
                
                if (localParsed.Id == request.Id)
                {
                    parsed = (localParsed, response.Buffer.Length);
                    break;
                }
            }

            return parsed;
        }

    }
}