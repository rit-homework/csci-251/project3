using System;
using System.Buffers.Binary;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace dns
{
    public class DatagramReader
    {
        private const byte ReferenceByte = 0b1100_0000;
        
        private readonly ArraySegment<byte> _data;
        private int _index = 0;
        
        private Span<byte> Span => _data;

        private Span<byte> TailSpan => Span.Slice(_index);

        public DatagramReader(byte[] data)
        {
            _data = data;
        }

        private DatagramReader(ArraySegment<byte> span, int startingIndex)
        {
            _data = span;
            _index = startingIndex;
        }

        public IPAddress ReadIpV4Addr()
        {
            var addr = new IPAddress(TailSpan.Slice(0, 4));
            _index += 4;
            return addr;
        }

        public IPAddress ReadIpV6Addr()
        {
            var addr = new IPAddress(TailSpan.Slice(0, 16));
            _index += 16;
            return addr;
        }

        public ushort ReadU16()
        {
            var num = BinaryPrimitives.ReadUInt16BigEndian(TailSpan);
            _index += 2;
            return num;
        }

        public uint ReadU32()
        {
            var num = BinaryPrimitives.ReadUInt32BigEndian(TailSpan);
            _index += 4;
            return num;
        }

        public byte ReadByte()
        {
            var num = _data[_index];
            _index += 1;
            return num;
        }

        public RecordType ReadType()
            => (RecordType) ReadU16();

        public ReadOnlySpan<byte> ReadBytes(int length)
        {
            var span = TailSpan.Slice(0, length);
            _index += length;
            return span;
        }

        public void Skip(int n)
        {
            _index += n;
        }

        public IList<byte[]> ReadLabels()
        {
            var result = new List<byte[]>();

            while (true)
            {
                var length = ReadByte();
                if (length == 0)
                {
                    break;
                }

                if ((length & ReferenceByte) != 0)
                {
                    var remainingLength = ReadByte();
                    var referencedIdx = (length & 0b00111111) << 8 | remainingLength;
                    
                    var subreader = new DatagramReader(_data, referencedIdx);
                    
                    var dereferenced = subreader.ReadLabels();
                    result.AddRange(dereferenced);

                    return result;
                }
                
                result.Add(ReadBytes(length).ToArray());
            }

            return result;
        }

        public string ReadDnsName()
        {
            var labels = ReadLabels();
            return String.Join(".", labels.Select(rawLabel => Encoding.UTF8.GetString(rawLabel)));
        }
    }
}