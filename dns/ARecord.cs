using System.Net;

namespace dns
{
    public class ARecord
    {
        public IPAddress Address { get; }

        public ARecord(IPAddress address)
        {
            Address = address;
        }
    }
}