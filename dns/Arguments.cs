using System;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;

namespace dns
{
    public class Arguments
    {
        private static readonly IPEndPoint ServerEndPoint = new IPEndPoint(IPAddress.Parse("8.8.8.8"), 53);

        private const string Usage = "Usage:\nnotdig <dns server> <A|AAAA|CNAME> hostname";
        
        public IPEndPoint DnsServer { get; }
        public RecordType Type { get; }
        public string ToLookup { get; }

        private Arguments(IPEndPoint dnsServer, RecordType type, string toLookup)
        {
            DnsServer = dnsServer;
            Type = type;
            ToLookup = toLookup;
        }
        
        public static Arguments Parse(string[] args)
        {
            if (args.Length < 1 || args.Length > 3)
            {
                Console.WriteLine(Usage);
                return null;
            }

            var toLookup = args[args.Length - 1];
            var type = args.Length >= 2 ? Enum.Parse<RecordType>(args[args.Length - 2]) : RecordType.A;
            var server = args.Length >= 3 ? ParseEndpoint(args[0]): GetDefaultDnsServer();
            
            return new  Arguments(server, type, toLookup);
        }

        private static IPEndPoint ParseEndpoint(string s)
        {
            var parts = s.Split(":");
            var addressPart = parts[0];
            var portPart = parts.Length >= 2 ? int.Parse(parts[1]) : 53;
            
            return new IPEndPoint(IPAddress.Parse(addressPart), portPart);
        }

        private static IPEndPoint GetDefaultDnsServer()
        {
            var address =
                NetworkInterface.GetAllNetworkInterfaces()
                    .Where(iface => iface.OperationalStatus == OperationalStatus.Up)
                    .Select(iface => iface.GetIPProperties().DnsAddresses)
                    .First()
                    .First();
            
            return new IPEndPoint(address, 53);
        }
    }
}