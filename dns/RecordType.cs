namespace dns
{
    public enum RecordType : ushort
    {
        A = 1,
        CNAME = 5,
        AAAA = 28,
    }
}